Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: compiz
Source: https://github.com/compiz-reloaded/compiz

Files: *
Copyright: David Reveman <davidr@novell.com>
           Radek Doulik <rodo@novell.com>
           Mirco Müller <macslow@bangang.de>
           Søren Sandmann <sandmann@redhat.com>
           Dan Winship <danw@novell.com>
           Brian Paul <brian.paul@tungstengraphics.com>
           Mike Cook <mcook@novell.com>
           Mike Dransfield <mike@blueroot.co.uk>
           Diogo Ferreira <diogo@underdev.org>
           gandalfn <gandalfn@club-internet.fr>
           Guillaume <ixcemix@gmail.com>
           Kristian Høgsberg <krh@redhat.com>
           Dennis Kasprzyk <onestone@beryl-project.org>
           Gerd Kohlberger <lowfi@chello.at>
           Volker Krause <vkrause@kde.org>
           moppsy <moppsy@comcast.net>
           Jeremy C. Reed <reed@reedmedia.net>
           Thierry Reding <thierry@gilfi.de>
           Julian Sikorski <lordzanon@poczta.onet.pl>
           Quinn Storm <livinglatexkali@gmail.com>
           Erkin Bahceci <erkinbah@gmail.com>
           SUSE LINUX GmbH, Nuernberg, Germany.
           Jakub Friedl <jfriedl@suse.cz>
           launchpad.net/compiz
           github.com/compiz-reloaded
           The Free Software Foundation, Inc.
License: NTP

Files: libdecoration/decoration.c plugins/compiz-decorator
Copyright: 2006 Novell, Inc.
           2015 Sorokin Alexei <sor.alexei@meowr.ru>
           2007 CyberOrg <cyberorg@cyberorg.info>
License: LGPL-2+

Files: plugins/place.c plugins/wall.c
Copyright: 2001 Havoc Pennington
           2002, 2003 Red Hat, Inc.
           2003 Rob Adams
           2005 Novell, Inc.
           2006 Robert Carr <racarr@beryl-project.org>
License: GPL-2+

Files: src/matrix.c
Copyright: 1999-2005  Brian Paul
License: MIT

Files: src/display.c
Copyright: 2005 Novell, Inc.
           Matthias Clasen
           Dominik Vogt
License: misc-1
 Permission to use, copy, modify, distribute, and sell this software
 and its documentation for any purpose is hereby granted without
 fee, provided that the above copyright notice appear in all copies
 and that both that copyright notice and this permission notice
 appear in supporting documentation, and that the name of
 Novell, Inc. not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior permission.
 Novell, Inc. makes no representations about the suitability of this
 software for any purpose. It is provided "as is" without express or
 implied warranty.
 .
 NOVELL, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 NO EVENT SHALL NOVELL, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 .
 The convertProperty and handleSelectionRequest functions
 are taken from the source code of fvwm2:
 .
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

Files: gtk-window-decorator/gtk-window-decorator.c
Copyright: 2006 Novell, Inc.
           Matthias Clasen
License: misc-2
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 Lesser General Public License for more details.
 .
 The convert_property and handle_selection_request functions
 are taken from the source code of fvwm2:
 .
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

Files: src/window.c
Copyright: 2005 Novell, Inc.
           Robert Nation
License: misc-3
 Permission to use, copy, modify, distribute, and sell this software
 and its documentation for any purpose is hereby granted without
 fee, provided that the above copyright notice appear in all copies
 and that both that copyright notice and this permission notice
 appear in supporting documentation, and that the name of
 Novell, Inc. not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior permission.
 Novell, Inc. makes no representations about the suitability of this
 software for any purpose. It is provided "as is" without express or
 implied warranty.
 .
 NOVELL, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 NO EVENT SHALL NOVELL, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 .
 You may use this code for any purpose, as long as the original
 copyright remains in the source code and all documentation

Files: debian/*
Copyright: 2006 2007 Thierry Reding <thierry@gilfi.de>
           2016 Jof Thibaut <compiz@tuxfamily.org>
License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: NTP
 Copyright © 2005 Novell, Inc.
 .
 Permission to use, copy, modify, distribute, and sell this software
 and its documentation for any purpose is hereby granted without
 fee, provided that the above copyright notice appear in all copies
 and that both that copyright notice and this permission notice
 appear in supporting documentation, and that the name of
 Novell, Inc. not be used in advertising or publicity pertaining to
 distribution of the software without specific, written prior permission.
 Novell, Inc. makes no representations about the suitability of this
 software for any purpose. It is provided "as is" without express or
 implied warranty.
 .
 NOVELL, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, 
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 NO EVENT SHALL NOVELL, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 2 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in `/usr/share/common-licenses/LGPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 BRIAN PAUL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
